<?php 

require_once("Connection.php");

  class Produto extends Connection {
    private $id;
    private $nome;
    private $sku;
    private $preco;
    private $descricao;
    private $quantidade;
    private $categoria;
    private $img;

    public function getId() { return $this->id; }

    public function getNome() { return $this->nome; }

    public function getSku() { return $this->sku; }

    public function getPreco() { return $this->preco; }

    public function getDescricao() { return $this->descricao; }

    public function getQuantidade() { return $this->quantidade; }

    public function getCategoria() { return $this->categoria; }

    public function getImg() { return $this->img; }

    public function setId($string)
    {
      $this->id = $string;
    }

    public function setNome($string)
    {
      $this->nome = $string;
    }

    public function setSku($string)
    {
      $this->sku = $string;
    }

    public function setPreco($string)
    {
      $this->preco = $string;
    }

    public function setDescricao($string)
    {
      $this->descricao = $string;
    }

    public function setQuantidade($string)
    {
      $this->quantidade = $string;
    }

    public function setCategoria($string)
    {
      $this->categoria = $string;
    }

    public function setImg($string)
    {
      $this->img = $string;
    }
  }